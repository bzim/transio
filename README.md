# Transio

Esoteric transitional language. [Details here](https://esolangs.org/wiki/Transio)

# Supported platforms

Any platform with support to Rust and std crate, including GNU/Linux, MacOS, BSDs, Windows, etc.

# Build from source and Install

Dependecies:
* Rust environment >= 1.26 (older versions may work, but not guaranteed)

Steps:

1. `git clone https://gitlab.com/bzim/transio`
2. `cd transio`
3. `cargo build --release`
* Running locally: `cargo run`
* Installing: `cargo install --path . --bin transio`
