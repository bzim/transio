extern crate transio;

use transio::{eval::compile, parser::Parser};
use std::{
    env::args,
    fs::File,
    io::{stdin, stdout, Read},
    process::exit,
};

fn main() {
    let mut args = args();
    args.next();
    let filepath = match args.next() {
        Some(path) => path,
        _ => {
            eprintln!("cli error: missing input file argument");
            exit(-1);
        },
    };

    let mut file = match File::open(&filepath) {
        Ok(file) => file,
        Err(e) => {
            eprintln!("error opening file {}: {}", filepath, e);
            exit(-1);
        },
    };

    let mut contents = Vec::new();

    if let Err(e) = file.read_to_end(&mut contents) {
        eprintln!("error reading file {}: {}", filepath, e);
        exit(-1);
    }

    let mut parser = Parser::new(&contents);
    let mut trans = Vec::new();
    loop {
        match parser.parse() {
            Ok(Some(t)) => trans.push(t),
            Ok(None) => break,
            Err(e) => {
                eprintln!("parsing error at line {} column {}", e.line, e.col);
                exit(-1);
            },
        }
    }

    let mut stdin = stdin();
    let mut stdout = stdout();

    let mut machine = compile(&trans, &mut stdin, &mut stdout);
    if let Err(e) = machine.run() {
        eprintln!("io error executing: {}", e);
        exit(-1);
    }
}
