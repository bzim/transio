pub struct Error {
    pub pos: usize,
    pub line: usize,
    pub col: usize,
}

pub struct Parser<'a> {
    src: &'a [u8],
    pos: usize,
    line: usize,
    col: usize,
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash)]
pub enum Rhs {
    Lit(u16),
    Reg(Box<[u8]>),
}

#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Hash, Default)]
pub struct Trans {
    pub lhs: Box<[u8]>,
    pub rhs: Rhs,
}

impl Default for Rhs {
    fn default() -> Self {
        Rhs::Lit(0)
    }
}

impl<'a> Parser<'a> {
    pub fn new(src: &'a [u8]) -> Self {
        Self {
            src,
            pos: 0,
            line: 1,
            col: 1,
        }
    }

    pub fn parse(&mut self) -> Result<Option<Trans>, Error> {
        self.skip_unimportant();
        if self.pos >= self.src.len() {
            return Ok(None);
        }
        let lhs = self.parse_lhs()?;
        self.skip_unimportant();
        self.skip_trans_optr()?;
        self.skip_unimportant();
        let rhs = self.parse_rhs()?;
        Ok(Some(Trans { lhs, rhs }))
    }

    pub fn error(&self) -> Error {
        Error {
            pos: self.pos,
            line: self.line,
            col: self.col,
        }
    }

    fn parse_lhs(&mut self) -> Result<Box<[u8]>, Error> {
        let mut ident = Vec::new();
        while let Some(&byte) = self.src.get(self.pos) {
            if (byte < b'a' || byte > b'z')
                && (byte < b'A' || byte > b'Z')
                && (byte < b'0' || byte > b'9')
                && byte != b'_'
            {
                break;
            }
            self.next();
            ident.push(byte);
        }
        if ident.len() == 0 {
            Err(self.error())
        } else {
            Ok(ident.into())
        }
    }

    fn skip_trans_optr(&mut self) -> Result<(), Error> {
        match self.src.get(self.pos) {
            Some(b'<') => (),
            _ => return Err(self.error()),
        }
        self.next();
        match self.src.get(self.pos) {
            Some(b'-') => (),
            _ => return Err(self.error()),
        }
        self.next();
        Ok(())
    }

    fn parse_rhs(&mut self) -> Result<Rhs, Error> {
        let peek = match self.src.get(self.pos) {
            Some(&byte) => byte,
            _ => return Err(self.error()),
        };
        if peek == b'$' {
            self.next();
            let mut res = 0;
            while let Some(&byte) = self.src.get(self.pos) {
                if byte >= b'0' && byte <= b'9' {
                    res = res << 4 | (byte - b'0') as u16;
                } else if byte >= b'A' && byte <= b'F' {
                    res = res << 4 | (byte - b'A' + 0xA) as u16;
                } else if byte >= b'a' && byte <= b'f' {
                    res = res << 4 | (byte - b'a' + 0xA) as u16;
                } else {
                    break;
                }
                self.next();
            }
            Ok(Rhs::Lit(res))
        } else {
            self.parse_lhs().map(Rhs::Reg)
        }
    }

    fn next(&mut self) {
        if self.pos < self.src.len() {
            if self.src[self.pos] == b'\n' {
                self.line += 1;
                self.col = 1;
            } else {
                self.col += 1;
            }
            self.pos += 1;
        }
    }

    fn skip_unimportant(&mut self) {
        while self.skip_whitespace() || self.skip_comments() {}
    }

    fn skip_comments(&mut self) -> bool {
        if self.pos < self.src.len() && self.src[self.pos] == b'#' {
            self.next();
            while self.pos < self.src.len() && self.src[self.pos] != b'\n' {
                self.next();
            }
            true
        } else {
            false
        }
    }

    fn skip_whitespace(&mut self) -> bool {
        let mut skipped = false;
        while self.pos < self.src.len()
            && (self.src[self.pos] == b' '
                || self.src[self.pos] == b'\n'
                || self.src[self.pos] == b'\t'
                || self.src[self.pos] == b'\r')
        {
            self.next();
            skipped = true;
        }
        skipped
    }
}
