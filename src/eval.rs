use parser as ast;
use std::{
    cmp::Ordering::*,
    collections::{HashMap, VecDeque},
    fmt,
    io,
};

struct Trans<'a, 'b, I, O>
where
    I: io::Read + 'a,
    O: io::Write + 'b,
{
    lhs: u16,
    rhs: u16,
    lptr: fn(&mut Machine<'a, 'b, I, O>, u16) -> io::Result<()>,
    rptr: fn(&mut Machine<'a, 'b, I, O>) -> io::Result<u16>,
}

#[derive(Debug)]
pub struct Machine<'a, 'b, I, O>
where
    I: io::Read + 'a,
    O: io::Write + 'b,
{
    input: &'a mut I,
    output: &'b mut O,
    ip: u16,
    trans: Box<[Trans<'a, 'b, I, O>]>,
    regs: Vec<u16>,
    deque_1: VecDeque<u16>,
    deque_2: VecDeque<u16>,
}

pub fn compile<'a, 'b, I, O>(
    ast: &[ast::Trans],
    input: &'a mut I,
    output: &'b mut O,
) -> Machine<'a, 'b, I, O>
where
    I: io::Read + 'a,
    O: io::Write + 'b,
{
    let mut trans = Vec::with_capacity(ast.len());
    let mut tbl = HashMap::new();
    let mut id = 0;

    for node in ast {
        let lhs: (u16, fn(&mut _, _) -> _) = match &*node.lhs {
            b"io" => (0, Machine::lhs_io),
            b"ip" => (0, Machine::lhs_ip),
            b"front1" => (0, Machine::lhs_front1),
            b"front2" => (0, Machine::lhs_front2),
            b"back1" => (0, Machine::lhs_back1),
            b"back2" => (0, Machine::lhs_back2),
            b"add" => (0, Machine::lhs_add),
            b"mul" => (0, Machine::lhs_mul),
            b"xor" => (0, Machine::lhs_xor),
            b"and" => (0, Machine::lhs_and),
            b"shl" => (0, Machine::lhs_shl),
            b"shr" => (0, Machine::lhs_shr),
            b"cmp" => (0, Machine::lhs_cmp),
            _ => {
                let inserted = *tbl.entry(node.lhs.clone()).or_insert(id);
                if inserted == id {
                    id += 1;
                }
                (inserted, Machine::lhs_reg)
            },
        };

        let rhs: (u16, fn(&mut _) -> _) = match &node.rhs {
            ast::Rhs::Lit(lit) => (*lit, Machine::rhs_lit),
            ast::Rhs::Reg(r) => match &**r {
                b"io" => (0, Machine::rhs_io),
                b"ip" => (0, Machine::rhs_ip),
                b"front1" => (0, Machine::rhs_front1),
                b"front2" => (0, Machine::rhs_front2),
                b"back1" => (0, Machine::rhs_back1),
                b"back2" => (0, Machine::rhs_back2),
                b"add" => (0, Machine::rhs_add),
                b"mul" => (0, Machine::rhs_mul),
                b"xor" => (0, Machine::rhs_xor),
                b"and" => (0, Machine::rhs_and),
                b"shl" => (0, Machine::rhs_shl),
                b"shr" => (0, Machine::rhs_shr),
                b"cmp" => (0, Machine::rhs_cmp),
                _ => {
                    let inserted = *tbl.entry(r.clone()).or_insert(id);
                    if inserted == id {
                        id += 1;
                    }
                    (inserted, Machine::rhs_reg)
                },
            },
        };

        trans.push(Trans {
            lhs: lhs.0,
            rhs: rhs.0,
            lptr: lhs.1,
            rptr: rhs.1,
        })
    }

    Machine {
        input,
        output,
        ip: 0,
        trans: trans.into(),
        regs: vec![0; id as usize],
        deque_1: VecDeque::new(),
        deque_2: VecDeque::new(),
    }
}

impl<'a, 'b, I, O> fmt::Debug for Trans<'a, 'b, I, O>
where
    I: io::Read + 'a,
    O: io::Write + 'b,
{
    fn fmt(&self, fmtr: &mut fmt::Formatter) -> fmt::Result {
        write!(
            fmtr,
            "{} lhs: {}, rhs: {}, lptr: {}, rptr: {} {}",
            '{',
            self.lhs,
            self.rhs,
            self.lptr as usize,
            self.rptr as usize,
            '}'
        )
    }
}

impl<'a, 'b, I, O> Clone for Trans<'a, 'b, I, O>
where
    I: io::Read + 'a,
    O: io::Write + 'b,
{
    fn clone(&self) -> Self {
        Self {
            lhs: self.lhs,
            rhs: self.rhs,
            lptr: self.lptr,
            rptr: self.rptr,
        }
    }
}

impl<'a, 'b, I, O> Copy for Trans<'a, 'b, I, O>
where
    I: io::Read + 'a,
    O: io::Write + 'b,
{}

impl<'a, 'b, I, O> Machine<'a, 'b, I, O>
where
    I: io::Read + 'a,
    O: io::Write + 'b,
{
    pub fn run(&mut self) -> io::Result<()> {
        while let Some(&trans) = self.trans.get(self.ip as usize) {
            let val = (trans.rptr)(self)?;
            (trans.lptr)(self, val)?;
            self.ip += 1;
        }
        Ok(())
    }

    pub fn reset(&mut self) {
        self.deque_1.clear();
        self.deque_2.clear();
        self.ip = 0;
        for reg in &mut self.regs {
            *reg = 0;
        }
    }

    fn rhs_lit(&mut self) -> io::Result<u16> {
        Ok(self.trans[self.ip as usize].rhs)
    }

    fn lhs_reg(&mut self, val: u16) -> io::Result<()> {
        let lhs = self.trans[self.ip as usize].lhs;
        self.regs[lhs as usize] = val;
        Ok(())
    }

    fn rhs_reg(&mut self) -> io::Result<u16> {
        let rhs = self.trans[self.ip as usize].rhs;
        Ok(self.regs[rhs as usize])
    }

    fn lhs_io(&mut self, val: u16) -> io::Result<()> {
        io::Write::write_all(&mut self.output, &[val as u8])
    }

    fn rhs_io(&mut self) -> io::Result<u16> {
        let mut buf = [0];
        match io::Read::read_exact(&mut self.input, &mut buf) {
            Ok(()) => Ok(buf[0] as u16),
            Err(e) => if e.kind() == io::ErrorKind::UnexpectedEof {
                Ok(!0)
            } else {
                Err(e)
            },
        }
    }

    fn lhs_ip(&mut self, target: u16) -> io::Result<()> {
        self.ip = target % (self.trans.len() + 1) as u16;
        Ok(())
    }

    fn rhs_ip(&mut self) -> io::Result<u16> {
        Ok(self.ip)
    }

    fn lhs_front1(&mut self, val: u16) -> io::Result<()> {
        self.deque_1.push_front(val);
        Ok(())
    }

    fn rhs_front1(&mut self) -> io::Result<u16> {
        Ok(self.deque_1.pop_front().unwrap_or(0))
    }

    fn lhs_front2(&mut self, val: u16) -> io::Result<()> {
        self.deque_2.push_front(val);
        Ok(())
    }

    fn rhs_front2(&mut self) -> io::Result<u16> {
        Ok(self.deque_2.pop_front().unwrap_or(0))
    }

    fn lhs_back1(&mut self, val: u16) -> io::Result<()> {
        self.deque_1.push_back(val);
        Ok(())
    }

    fn rhs_back1(&mut self) -> io::Result<u16> {
        Ok(self.deque_1.pop_back().unwrap_or(0))
    }

    fn lhs_back2(&mut self, val: u16) -> io::Result<()> {
        self.deque_2.push_back(val);
        Ok(())
    }

    fn rhs_back2(&mut self) -> io::Result<u16> {
        Ok(self.deque_2.pop_back().unwrap_or(0))
    }

    fn lhs_add(&mut self, val: u16) -> io::Result<()> {
        let x = self.deque_1.pop_front().unwrap_or(0);
        self.deque_1.push_front(x.wrapping_add(val));
        Ok(())
    }

    fn rhs_add(&mut self) -> io::Result<u16> {
        let x = self.deque_1.pop_front().unwrap_or(0);
        let y = self.deque_1.pop_front().unwrap_or(0);
        Ok(x.wrapping_add(y))
    }

    fn lhs_mul(&mut self, val: u16) -> io::Result<()> {
        let x = self.deque_1.pop_front().unwrap_or(0);
        self.deque_1.push_front(x.wrapping_mul(val));
        Ok(())
    }

    fn rhs_mul(&mut self) -> io::Result<u16> {
        let x = self.deque_1.pop_front().unwrap_or(0);
        let y = self.deque_1.pop_front().unwrap_or(0);
        Ok(x.wrapping_mul(y))
    }

    fn lhs_xor(&mut self, val: u16) -> io::Result<()> {
        let x = self.deque_1.pop_front().unwrap_or(0);
        self.deque_1.push_front(x ^ val);
        Ok(())
    }

    fn rhs_xor(&mut self) -> io::Result<u16> {
        let x = self.deque_1.pop_front().unwrap_or(0);
        let y = self.deque_1.pop_front().unwrap_or(0);
        Ok(x ^ y)
    }

    fn lhs_and(&mut self, val: u16) -> io::Result<()> {
        let x = self.deque_1.pop_front().unwrap_or(0);
        self.deque_1.push_front(x & val);
        Ok(())
    }

    fn rhs_and(&mut self) -> io::Result<u16> {
        let x = self.deque_1.pop_front().unwrap_or(0);
        let y = self.deque_1.pop_front().unwrap_or(0);
        Ok(x & y)
    }

    fn lhs_shl(&mut self, val: u16) -> io::Result<()> {
        let x = self.deque_1.pop_front().unwrap_or(0);
        self.deque_1
            .push_front(x.checked_shl(val as u32).unwrap_or(0));
        Ok(())
    }

    fn rhs_shl(&mut self) -> io::Result<u16> {
        let x = self.deque_1.pop_front().unwrap_or(0);
        let y = self.deque_1.pop_front().unwrap_or(0);
        Ok(y.checked_shl(x as u32).unwrap_or(0))
    }

    fn lhs_shr(&mut self, val: u16) -> io::Result<()> {
        let x = self.deque_1.pop_front().unwrap_or(0);
        self.deque_1
            .push_front(x.checked_shr(val as u32).unwrap_or(0));
        Ok(())
    }

    fn rhs_shr(&mut self) -> io::Result<u16> {
        let x = self.deque_1.pop_front().unwrap_or(0);
        let y = self.deque_1.pop_front().unwrap_or(0);
        Ok(y.checked_shr(x as u32).unwrap_or(0))
    }

    fn lhs_cmp(&mut self, val: u16) -> io::Result<()> {
        let x = self.deque_1.pop_front().unwrap_or(0);
        self.deque_1.push_front(match x.cmp(&val) {
            Equal => 0,
            Less => !0,
            Greater => 1,
        });
        Ok(())
    }

    fn rhs_cmp(&mut self) -> io::Result<u16> {
        let x = self.deque_1.pop_front().unwrap_or(0);
        let y = self.deque_1.pop_front().unwrap_or(0);
        Ok(match y.cmp(&x) {
            Equal => 0,
            Less => !0,
            Greater => 1,
        })
    }
}
